var email   = require("emailjs");
var server  = email.server.connect({
   user:    "me@talks.vn", 
   password:"password", 
   host:    "box.talks.vn", 
   port: 25,
   ssl:     false
});

// send the message and get a callback with an error or details of the message that was sent
server.send({
   text:    "i hope this works", 
   from:    "me@talks.vn", 
   to:      "sample@talks.vn",
   subject: "testing emailjs",
   body: "Test body"
}, function(err, message) { console.log(err || message); });